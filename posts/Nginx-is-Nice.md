Nginx is Nice
2018-12-21 13:56:58.369212568 UTC
Post

Just a short post. Got an urgent need to run one of my toy project in a random
vps. But because I am a cheapskate, I opted the cheapest plan. The problem is
(or was), that darn server is sooooo slow. I left it for a couple of hours, but
that thing was still compiling the library. So, I decided to use that vps as
a proxy or something like that and let another vps instance runs the warp server
app. And because of one thing and another, I can't directly point my dns zone
thingy to the vps instance that runs the warp server.

So, what did I do?

Just create an nginx config like this.

```
server {
  listen 80;
  server_name localhost;

  location / {
    # snip
   proxy_redirect http://real.warp.server http://my.toy.project;
   proxy_set_header host http://real.warp.server;
   proxy_set_header X-real-ip $remote_addr;
   proxy_set_header X-forward-for $proxy_add_x_forwarded_for;
   proxy_pass http://real.warp.server;
  }
}
```

And that's it. Life is pretty good, I guess.
