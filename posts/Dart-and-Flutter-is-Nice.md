Dart and Flutter is Nice
2018-12-22 15:58:17.58985901 UTC
Post

I just want to talk about my experience in mobile development using Dart and Flutter.
The background is pretty simple. Remember a few other posts about public well
project? I've completed the last part, mobile client for the dudes who record
the water usages. Feels pretty good for finishing this program, my dude.

Now, enter Dart and Flutter. To be really honest, I am a colossal dunce when it
comes to ALGOL family language. Not that I'm not a dunce in any other family.
But, I don't think I can model my mind to conform that kind of programming paradigm.
So, back to Dart. My first (and current image) of Dart is: it's just another
X-to-JavaScript language. I know, I know, Dart has a virtual machine of its own.
There are a few nice things that I want to list about it:

- Named params and props are nice. I really wish my current favorite language
  support it out of the box.
- Async process is nice, I guess. Though monad concept helped me to understand it.
- "Fat arrows". You know, I don't really like curly braces.
- IDE-like supports. I don't really like Intellij and/or AndroidStudio because of
  their memory consumption and weight. So, I use VSCode and some extensions like
  Dart and Flutter. In 5 days I wrote that program, the support was very satisfying.

Now, there are a few things that I don't think I really like. And to remind you,
it's a case of "it's not you, it's me" problem.

- Immutability, I guess. There were a few times where I consider I made bad
  decisions because not only I'm a dunce who can only copy paste from internet,
  but also forgot that I can mutate a global variable in the code base.
- Formatting. I mean, come on.
```
              ),
            ),
          ],
        ),
      ),
    );
  }
}
```
- [insert standard complaint about strong type or something like a smug weenie here]

How about Flutter, you say. Again, it's nice. It lets me write a simple android
program which talks to internet and has auth process in less than 20 hours by
a complete retard like me. My better experience in writing android program was
almost 3 years ago, in Kotlin, using maps or something like, and ended up in failure
because upper dude decided to abandon that "project".
lol.

Let's write the nice things about Flutter which is an extension from Dart's nice
things.

- Declarative UI like Anko and Scaloid which is really really nice. It surely is
  easier to the eyes than staring into a overly verbose UI definition in XML.
- Hot reload! There's no need to invoke gradle or something along that line which
  makes my cpu screams in terror whenever I change the a line or two.
- Command line thingy. I can use `make` to build and upload it to my site!

Now for the "meh" part, where mostly not Flutter's fault, there are a few things:

- Android development in and of itself. It's Greek to me. Unlike my usual language
  which I've invested hundred of hours studying, I mostly just skim the docs and
  cookbooks.
- I don't know, but I guess Flutter cannot publish a single apk where it contains
  files for arm64 and arm32 because my old man's friend told me that he couldn't
  install the output from `flutter build apk`.
- Again, Android dev. in and of itself.

Pretty much that's it. I guess. And considering how little investment I've put
into it, more or less 20 hours, I can say that I had a really nice experience
in building something using Flutter.
Will I use Flutter again? Given choice and opportunity, absolutely. Unless eta-lang's
support for Android dev. has already taken off at that moment.
