(Bahasa) Nulis Firmware
2019-12-05 23:45:06.08400259 UTC
Post

[Kemarin sudah nggambar pcb](2019-11-27--bahasa--nggambar-kibor.html) kan, bang.
Sekarang waktunya nulis firmware untuk pcb kemarin.

Sebelumnya, mari kita pasang beberapa software.
Bila abang menggunakan windows, mohon install [msys2](http://www.msys2.org/) dulu.
Setelah msys2 terpasang, silakan abang buka program tersebut.
Namun bila abang menggunakan sistem operasi semacam debian/ubuntu atau arch/manjaro,
tinggal buka emulator terminal favorit abang.
Pun halnya dengan abang yang menggunakan punya apel, silakan buka emulator terminal.

Mari kita pasang `git` biar gaul, bang.
Untuk arch/manjaro/windows (msys2), tinggal `pacman -Syu --noconfirm git`.
Sistem operasi / distro lain, silakan menyesuaikan.
Setelah terinstall, abang masuppkan

```
git clone --recurse-submodule https://github.com/qmk/qmk_firmware
```
yang berarti abang berniat untuk 'menyalin' semua isi lumbung kode tersebut ke
komputer abang.
Dilanjutkan dengan memasukkan perintah `cd qmk_firmware` untuk berpindah ke direktori
utama dari `qmk_firmware`.

Sekarang waktunya untuk masang masang lainnya dengan memasukkan

```
util/qmk_install.sh
```

yang berarti komputer suhu akan meng-eksek berkas `qmk_install.sh` di direktori
`util` tanpa SSI terlebih dahulu.
Sebenernya, `qmk_install.sh` sendiri cuma berupa skrip yang memanggil skrip lain
yang sesuai dengan sistem operasi suhu.

Untuk memeriksa apakah hasil eksek suhu berhasil atau tidak, tolong suhu masuppkan
perintah

```
make claw44:default
```

Bila berhasil, suhu sudah berhasil menurunkan mesin untuk kibor `claw44` khususnya
keymap `default`.
Bilamana suhu gagal, coba pakai `sudo` di depan `make`.
Kalau masih gagal, ada google, hu.

Ok, lanjutkeun ke bagian berikutnya dengan memasuppkan.

```
util/new_keyboard.sh
```

yang bakal meminta abang untuk memasupkan nama kibor yang ingin abang buat firmwarenya,
jenis prosesor, dan nama abang.
Untuk prosesor, pakai avr karena pcb kemarin menggunakan promicro.

## OWKOWKWOKWO

Sekarang abang pindah ke `keyboards/nama_kibor` menggunakan `cd`.
Di direktori tersebut, ada beberapa berkas dan direktori.
Antara lain:

### Direktori `keymaps`

Direktori berisi *keymap* bawaan untuk setiap kibor.
Setidaknya, harus ada satu direktori yang berisi `keymap.c`.
Kita akan kembali ke topik ini beberapa waktu ke depan.

### `config.h`

Berkas ini berisi mengenai konfigurasi konfigurasi yang digunakan oleh kibor abang.
Beberapa pilihan yang digunakan / diubah saat menulis *firmware* ini.
Antara lain:

- `#define VENDOR_ID 0x1234` dimana `0x1234` merupakan identitas vendor.
  Tidak ada aturan khusus dalam menentukan nilai ini.
  Hanya saja, sangat diharapkan abang untuk menggunakan nilai yang tidak / belum
  digunakan oleh vendor lain.
- `#define PRODUCT_ID 0x5678` dimana `0x5678` merupakan identitas produk.
  Sama halnya dengan `VENDOR_ID`, tidak ada aturan khusus dalam menentukan nilai
  ini.
  Hanya saja, sangat diharapkan abang untuk menggunakan nilai yang tidak / belum
  digunakan oleh produk lain lain.
- `#define MANUFACTURER ibnuda` dimana `ibnuda` itu saya.
  Silakan abang gunakan nama abang sendiri bila ingin.
- `#define PRODUCT Haish` dimana `Haish` merupakan nama kibor ini.
- `#define DESCRIPTION deskripsi` yang saya kira cukup jelas.
- `#define MATRIX_ROWS 4` merupakan jumlah baris yang ada pada kibor ini.
- `#define MATRIX_COLS 12` merupakan jumlah kolom yang ada pada kibor ini.
- `#define MATRIX_ROW_PINS { F4, F5, F6, F7 }` merupakan pin yang digunakan
  oleh kibor ini pada bagian baris, dari atas (baris `qwerty`) ke bawah (*modifiers*). 
  Untuk menentukan nilai di dalam kurung kurawal, silakan abang lihat kembali
  skematik dan/atau berkas pcb.
  Terutama pada bagian promicro.
- `#define MATRIX_COL_PINS { D1, D0, D4, C6, D7, E6, B4, B5, B6, B2, B3, B1 }`
  merupakan pin yang digunakan oleh kibor ini pada bagian kolom, dari kiri
  (`tab`, `capslock`, `shift`) ke kanan (`bspc`, `enter`, dkk).
  Untuk menentukan nilai di dalam kurung kurawal, silakan abang lihat kembali
  skematik dan/atau berkas pcb.
  Terutama pada bagian promicro.
- `#define DIODE_DIRECTION COL2ROW` secara gampang, merupakan arah strip hitam
  pada dioda di kibor abang.
  Untuk menentukan nilai ini, silakan abang perhatikan kembali skematik pada
  bagian dioda.
  *Firmware* ini menggunakan `COL2ROW` dikarenakan strip hitam menyambung ke arah
  baris.

### `haish.c`  (Sesuai Nama Kibor)

Berkas ini bisa dibilang sebagai 'inti' kedua dari kibor ini.
Inti yang saya maksud disini adalah, bila abang sunting berkas ini, apa yang
terjadi saat kibor pertama kali akan diproses disini.
Namun, bila tidak abang utak atik, maka pengaturan bawaan qmk sudah lebih dari
cukup.
Dan, untuk keperluan contoh kali ini, ini nggak perlu sama sekali.

### `haish.h` (Sesuai Nama Kibor)

Berkas ini merupakan representasi visual atas matriks dan penataan *keymap* abang.
Setidaknya, ada satu makro yang merepresentasikan matriks kibor.
Dan berdasarkan panduan dari QMK, makro tersebut berawalan `LAYOUT`.
Berikut contoh penjelasannya.

```
#define LAYOUT(\
  K00, K01, K02, K03, K04, K05, K06, K07, K08, K09, K0a, K0b, \
  K10, K11, K12, K13, K14, K15, K16, K17, K18, K19, K1a, K1b, \
  K20, K21, K22, K23, K24, K25, K26, K27, K28, K29, K2a, K2b, \
  K30, K31, K32,    K33,   K35,   K37,    K38, K39, K3a, K3b  \
) \
{ \
  { K00, K01, K02, K03, K04, K05, K06, K07, K08, K09, K0a, K0b }, \
  { K10, K11, K12, K13, K14, K15, K16, K17, K18, K19, K1a, K1b }, \
  { K20, K21, K22, K23, K24, K25, K26, K27, K28, K29, K2a, K2b }, \
  { K30, K31, K32, K33, KNO, K35, KNO, K37, K38, K39, K3a, K3b }  \
}
```
potongan di atas merupakan makro yang digunakan sebagai representasi dari layout
yang digunakan oleh mikrokontroler untuk menerjemahkan dari.

Haruskah saya harus menjelaskan soal gimana makro diproses lalu digunakan?
Saya kira nggak perlu karena ribet.

Ok, pada intinya, bagian di dalam kurung berhubungan dengan *keymap* abang dan
bagian di dalam kurung kurawal berhubungan dengan skematik pencetan-dioda kicad
abang.
Seperti sebagaimana yang abang lihat bahwa ada 4 baris dan 12 kolom baik di skematik
maupun pada bagian dalam kurung kurawal.
Implikasi dari pengaturan semacam ini adalah bilamana abang melukir posisi, misal,
`k01` dan `k11` pada bagian dalam kurung, saat abang menggunakan `LAYOUT` di
`keymap.c`, abang juga akan kebingungan karena posisi (asumsi *layout* standar
qwerty) `q` dan `a` terbalik.

### `rules.mk`

Berisi fitur-fitur yang bisa diatur untuk kibor ini.
Misalkan:

- `MOUSEKEY_ENABLE` diset menjadi `yes` bila abang ingin menggunakan fitur tikus
  pada kibor abang.
- `LEADER_ENABLE` diset menjadi `yes` bila abang ingin menggunakan makro atau semacamnya.
- `COMBO_ENABLE` diset menjadi `yes` bila abang ingin menggunakan kombo (misal
  pencet `a` dan `b` bersamaan keluarnya `enter`)

Secara umum, abang tidak perlu menyentuh berkas ini bila penggunaan kibor abang
tidak terlalu macam macam.
Saya sendiri, menggunakan beberapa fitur seperti `LEADER_ENABLE` dan `COMBO_ENABLE`.

OK.
Secara umum, semua berkas pada bagian atas direktori kibor sudah saya jelaskan.
Saatnya kita membuat `keymap.c`

### `keymaps/default/keymap.c`

Berkas ini merupakan berkas konfigurasi *keymap* abang.
Oh, dan semua potongan kode di sub-seksi ini merupakan bagian dari satu berkas utuh.

```
#include QMK_KEYBOARD_H
```
Merupakan deklarasi bahwa kita menggunakan berkas *header* yang berisi definisi
`LAYOUT` atau dalam kata lain `haish.h`.
Darimana kita tahu bahwa kita menggunakan berkas tersebut?
Tentu dari baca [konfigurasi kode sumber qmk](https://github.com/qmk/qmk_firmware/blob/master/build_keyboard.mk#L205).

```
#define _DASAR   0
#define _SIMBOL  1
#define _JARANG  2
#define _LAINNYA 3
```
Sekarang kita mendefinisikan 4 nilai yang akan kita gunakan sebagai *layer* pada
kibor.
Dimana:

- `_DASAR` merupakan *layer* standar QWERTY.
- `_SIMBOL` merupakan *layer* berisi simbol dan angka.
- `_JARANG` merupakan *layer* berisi pencetan yang jarang digunakan.
  Setidaknya bagi saya.
- `_LAINNYA` merupakan *layer* berisi `RESET` dan lain lain.
  Suka suka abang, lah.

```
#define SIMBOL  MO(_SIMBOL)
#define JARANG  MO(_JARANG)
#define LAINNYA MO(_LAINNYA)
```

Sedangkan untuk ketiga definisi di atas, cuma biar gampang aja, bang.

```
#define AL_D ALT_T(KC_F)
#define AL_J ALT_T(KC_J)
```

Kita mendefinisikan sebuah simbol baru untuk perilaku sebuah pencetan yang bila
dipencet kurang dari sekian waktu, akan mengirim `F` (atau `J`).
Namun bila abang menekan lebih dari sekian waktu, akan mengirim `ALT`

```
#define CT_S CTL_T(KC_S)
#define CT_L CTL_T(KC_L)
```

Sama dengan penjelasan sebelumnya, namun mengirim `CTRL` bukan `ALT`.

```
#define SH_S SFT_T(KC_S)
#define SH_L SFT_T(KC_L)
```

Mirip, namun bedanya bukan `CTRL` namun `SHIFT`.

```
#define CT_ESC CTL_T(KC_ESC)
```

Mari kita lanjutkan dengan menulis *layout* yang abang pencet pencet.

```
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  LAYOUT(
    KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_BSPC,
    CT_ESC,  KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT,
    KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_UP,
    KC_LCTL, KC_LALT, KC_LGUI,    KC_ENT,      SIMBOL,            KC_SPC,          KC_APP, KC_LEFT, KC_DOWN,   KC_RGHT
  ),

```

Potongan kode di atas merupakan definisi matriks *layout* yang sesuai dengan gambar
di artikel sebelumnya.

Dah ah, kapan kapan lagi kalau sempat dan niat.
Terutama untuk soal split dkk.
