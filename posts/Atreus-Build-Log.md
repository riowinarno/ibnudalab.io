Atreus Build Log
2018-10-16 02:36:45.320735 UTC
Post

I don't know since when, but I've been into mech. keyboards list half year.
At first, I bought a standard, cheap ass mech keyboard with mx blue switches.
A few months later, I bought the second one, ducky one with mx green switches.
Overall, the experience was really nice, apart from the frowns from my coworkers
(I guess).

And for the last two months, I lost to my headlessness and slaughtered my wallet
just to buy carpentry tools and stuff like that, just to build an [atreus](https://atreus.technomancy.us) keyboard.
What's worse, I lost to my headlessness even more shamefully when I ordered *two*
atreus switch layers.

But well, everything is usable now.

![Almost finished product](https://i.imgur.com/9CXNGKv.jpg)

I use it in my office, and so far, I sense no frowns at all.
Anyway, let's start with this thing.

## Bill of Materials

Actually, not much.

- 1 (one) unit of 5mm laser cut acrylic switch plate, procured for Rp. 75.000,00.
- 37 (thirty seven) units of Kailh BOX Burnt Orange @ Rp. 4.500,00.
- 5 (five) units of Kailh BOX Dark Yellow @ Rp. 4.500,00.
- 1 (one) unit of Atmega32u4 clone @ Rp. 78.000,00.
- 42 (forty two) units of diode 1N4148 @ Rp. 150,00.
- 4 (four) metres of AWG 24 @ 1.750,00.

and a few plates of plywoods lying around in my room.

## Soldering and Stuff

When I build this keyboard, there are a few steps that I took,

- soldering the diodes
- soldering the row cables
- attaching the row and column cables to pro micro
- flashing pro micro

First step: soldering the diodes.
It was pretty easy, apart from my shaky right hand, of course.

I soldered the left side first, and then followed by soldering the right side.
![Left side](https://i.imgur.com/ECWXGZy.jpg)

![Right side](https://i.imgur.com/uu0dpwq.jpg)

In a hindsight, I think I should have aligned it downward like [this build log](https://bergie.iki.fi/blog/atreus-build-log/).

Second step: soldering the row cables.
This one, was pretty easy too. But because of my retarded measuring phase,
the length is not uniform at all.

![Rows](https://i.imgur.com/BAyhCQs.jpg)

Third step: messing the promicro up. for real tho.
This one is the hardest thing, I swear. I spent three days (16 hours or so) just
to finish this part. I'm so ashamed of myself. I wish I didn't joke around in
electronic lecture.

![Pro Micro and cables](https://i.imgur.com/n6d7TcI.jpg)

Fourth step: connecting them all.
Actually, I rearrange them a bit. And the wires still falls off from time to time.

![Something](https://i.imgur.com/la8ugcS.jpg)

Connecting  the rows and columns (matrix) cables to the pro micro (assuming you're
using standard QWERTY layout).

- Row: Modifier keys (Fn, Ctrl, Alt, Tab, etc) to the pin `D3` which labeled by `TX0`.
- Row: `ZXCVB` to the pin `D2` which labeled by `RX1`.
- Row: `ASDFG` to the pin `D1` which labeled by `2`.
- Row: `QWERT` to the pin `D4` which labeled by `4`.
- Column: `QAZ` to the pin `F6` which labeled by `A1`
- Column: `WSX` to the pin `F7` which labeled by `A0`
- Column: `EDC` to the pin `B1` which labeled by `15`
- Column: `RFV` to the pin `B3` which labeled by `14`
- Column: `TGB` to the pin `B2` which labeled by `16`
- Column: `Ctrl Alt` to the pin `B6` which labeled by `10`
- Column: `YHN` to the pin `B5` which labeled by `9`
- Column: `UJM` to the pin `B4` which labeled by `8`
- Column: `IKComma` to the pin `E6` which labeled by `7`
- Column: `OLDot` to the pin `C6` which labeled by `5`
- Column: `PQuotSlash` to the pin `D7` which labeled by `6`

Fifth step: something related to firmware. Just look at this [commit](https://github.com/ibnuda/qmk_firmware/commit/0e3c7141c50723cfcba3312477ecee9e4c2ed79e).

At `config.h` in `keyboard/atreus` directory, I've changed a few lines:
```
#define MATRIX_ROW_PINS { D4, D1, D2, D3 }
#define MATRIX_COL_PINS { F6, F7, B1, B3, B2, B6, B5, B4, E6, C6, D7 }

```

And swapping `;` and `'` keys from Dvorak layout because of habit from [this](https://www.kaufmann.no/roland/dvorak/)
4layout.
I created a new `keymaps.c` in a new directory named `cacat` in `keyboards/atreus`
directory. The main difference is this part where I deleted qwerty layout and
replace it with Dvorak, just like I've said above.

```
+ [_DV] = LAYOUT(
+   KC_SCLN, KC_COMM, KC_DOT,  KC_P,    KC_Y,                      KC_F,    KC_G,    KC_C,    KC_R,    KC_L ,
+   KC_A,    KC_O,    KC_E,    KC_U,    KC_I,                      KC_D,    KC_H,    KC_T,    KC_N,    KC_S ,
+   KC_QUOT, KC_Q,    KC_J,    KC_K,    KC_X,                      KC_B,    KC_M,    KC_W,    KC_V,    KC_Z ,
+   KC_ESC,  KC_TAB, KC_LGUI,  KC_LSFT, KC_BSPC, KC_LALT, KC_LCTL, KC_SPC,  MO(_RS), KC_MINS, KC_SLSH, KC_ENT
+ ),

- [_QW] = LAYOUT( /* Qwerty */
-   KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,                      KC_Y,    KC_U,    KC_I,    KC_O,    KC_P    ,
-   KC_A,    KC_S,    KC_D,    KC_F,    KC_G,                      KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN ,
-   KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,                      KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH ,
-   KC_ESC, KC_TAB, KC_LGUI,  KC_LSFT, KC_BSPC,  KC_LCTL, KC_LALT, KC_SPC,  MO(_RS), KC_MINS, KC_QUOT, KC_ENT
- ),

```

Sixth step: flashing the promicro

- attach the pro micro to computer
- in the `qmk_firmwared` directory, input `make atreus:cacat:avrdude`
- get an error because of `gcc-8.2` incompatibility
- fix it by replacing this like

```
-uint32_t reset_key  __attribute__ ((section (".noinit")));
+uint32_t reset_key  __attribute__ ((section (".noinit,\"aw\",@nobits;")));

``` 

- reflash it
- reset pro micro
- press your keys one by one to check it out.


## Finish

It's a nice keyboard, based on my three days of usage in my office. It's quiet,
small, and everything is reachable without lifting my hands.
