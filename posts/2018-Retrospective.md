2018 Retrospective
2018-12-31 03:00:13.127072912 UTC
Post

Almost 2019 it seems. If I give a little thought of 2018, it seems that I've done
much more compared to 2017. Mainly because almost all of my spare time went solely
to myself and computers. Somewhat bitter-sweet, lol.

There are a few notable things for me. And, again, mostly related to computers.

## Put Haskell to Production

I've put a program which was written in Haskell at $WORK. AFAICT, my $WORK is
the first company 'round here that put Haskell into production. I've also learnt
a lot from this process. Though mostly not directly related to technical aspect.

## New Hobby

If I remember correctly, I wanted to have a mechanical keyboard circa 2015.
And this year, I decided to actually do that. One thing leads to another, now
I'm building my fourth mech. keyboard. What the actual fuck, yeah. That's
excessive. And, because I dislike gaudy things, I keep my keyboards as simple
as possible.

## Translated a Book

I wrote a Scala application for my graduation req. So, I somewhat have a softspot
to that language. By chance, I saw Mr. Halliday's book about FP in Scala and,
"Screw it! Let's translate that book for shit and giggle!", I said to myself
in my building's toilet. I've learned a lot of things in the process. Not only
I can be categorised as an uneducated dude, but also I've never read computer
science literature in my mother tongue. All in all, it was a nice experience.

## Side Projects

I don't know, I guess I need something to think about when $WORK is not really nice.
Fortunately, my old man had something for me to do. Though practically I spend
some pennies here and there because of this thing, it's alright. I like playing
around with something new like Elm and Dart.

## Not so Personal

### My Cat is A Prick

Well, I've spent almost 5.000.000 just for his food. Yeah, what the fuck indeed.
Not only that, he also doesn't want to stay still whenever I am home. What a prick!
I still love him, tho!

### Music

Or perhaps ambient sounds. Lately, I tend to put Shaykh Nazim (may God be pleased with him)
, Shaykh Hisham, and Dr. Nour Kabbani's suhbats when I do something.

### RIP Terry

May he found the peace he couldn't find in this life and write the network stack
directly to God in His heaven.

Also F U CIA!

## For 2019

### Marketable Things

It seems that I have to have marketable skills like JavaScript or Go or whatever.
You know, CurrentHipTechShit™. Perhaps I will try to port some of my existing
toys to Go or Rust.

### GUI

I want to learn GUI programming. Preferably something native and cross platform.
So, fltk is at the top list. Perhaps, I will try to write a 4chin or rebbit client.
idk.

### Brainfuck Thingy

Not interpreter, but a compiler. Though probably it will take more than 6 months
of spare time.

## ...

Basically that's it. Yeah, I did not much but arguably much more than the previous
year. I have nothing to expect, by the way.
