(Bahasa) Buku: Pemrog. Fungsional untuk Rakyat Jelata dengan Scalaz
2018-10-28 10:28:19.602178858 UTC
Post

Semenjak beberapa bulan lalu, saya menyibukkan diri dengan menerjemahkan buku
[Functional Programming for Mortals with Scalaz](https://leanpub.com/fpmortals).
Sebenarnya, saya tak punya maksud apa-apa dengan penerjemahan buku tersebut.
Pertama, saya tak menggunakan Scala di keseharian saya, baik dalam konteks
professional maupun personal. Kedua, saya sendiri tak begitu mengenal Scala.
Terakhir kali menggunakan Scala secara (agak) "serius" mungkin 3 tahun lalu
sebagai edukasi.

Pengalaman selama menerjemahkan buku ini kurang lebih menyenangkan. Saya yang
mempunyai latar belakang sedikit Haskell, saya mendapatkan sudut pandang yang
tak pernah saya kira dapatkan. Terang saja, saya tak mendapatkan pendidikan
formal dalam teori kategori dan teman-temannya. Namun, bukan berarti saya buta
akan `ReaderT` maupun pustaka transformator monad.

Sedangkan untuk proses penerjemahan sendiri, terutama karena saya amatir,
hanya dilakukan kalimat per kalimat. Saya takut mengubah konteks dengan melakukan
parafrase kalimat tersebut. Lalu untuk akurasi, seberapa yakin saya sendiri
dengan akurasi informasi yang saya terjemahkan? Mungkin 95%. Hanya satu atau
dua kalimat yang saya tak yakin keakuratannya. Dan kenapa saya tak meminta
bantuan? Biar asik saja.

Lalu, bila ingin melihat buku ini, silakan klik link dengan tujuan
[leanpub - fpmortals-id](https://leanpub.com/fpmortals-id). Bisa memilih untuk
tidak bayar, dan saya sarankan juga tidak bayar karena lebih baik sumbangkan
uang tersebut ke yang membutuhkan.
