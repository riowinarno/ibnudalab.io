Unlock OpenDS Account
2019-03-03 15:13:35.769611718 UTC
Post

Got a problem at $WORK. A somewhat old program went down because of the account
it uses got locked. God knows what happened.

Usually, a problem like this can be easily searched in less than 5 minutes.
But, this time, because of Oracle killed the OpenDS project, and there are barely
any traces of this project, I had to do it dirtily. (I know, I know, there's OpenDJ
but it was a modified opends 2.2 or something along that version. So yeah, an old
program uses a modified version of a dead project. I'm not annoyed at all, swear!)

First, I shut this ldap program down. Then, I went to `config/` directory and
opened `config.ldif`.  Followed by looking for some lines which I consider as
the `lock`ing statement
that damn config file.

```
dn: cn=Directory Manager,cn=Root DNs,cn=config
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
objectClass: top
objectClass: ds-cfg-root-dn-user
userpassword: {SSHA512}somelongpasswordyouknowwhatever
givenName: Directory
cn: Directory Manager
ds-cfg-alternate-bind-dn: cn=Manager
sn: Manager
ds-pwp-password-policy-dn: cn=Root Password Policy,cn=Password Policies,cn=config
ds-rlim-lookthrough-limit: 0
pwdFailureTime: 20190220042829.690Z
pwdFailureTime: 20190220042850.766Z
pwdFailureTime: 20190220043413.427Z
ds-rlim-time-limit: 0
ds-rlim-idle-time-limit: 0
modifyTimestamp: 20190220043413Z
ds-rlim-size-limit: 0
modifiersName: cn=Internal Client,cn=Root DNs,cn=config
pwdAccountLockedTime: 20190220043413.427Z

```
As you can see, there are a few information that can be found in that snippet.

1. Something tried to log in three times with incorrect passwords.
2. The account was locked at the last line.

I said to myself, "whatever, partner. i'm hungry. it's lunch time already." and
deleted that `pwdFailureTime` lines and that `pwdAccountLockedTime` line.
Sure, after I restarted that program, input the right password, i went smoothly.
Such is life of a janitor.
