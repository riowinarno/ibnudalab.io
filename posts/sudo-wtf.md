sudo wtf
2019-02-15 18:47:13.535613354 UTC
Post

I don't know what happened, but whenever I use `sudo`, it will show this:
```
$ sudo -l
Sorry, probeer het opnieuw.
Sorry, probeer het opnieuw.
sudo: 3 verkeerde wachtwoordpogingen
```
but surely something went wrong with pam or whatever they call it. Let's check
'em out!
```
[root@melati pam.d]# ls
chage      chpasswd  groupdel   i3lock  newusers  polkit-1    rsh        sddm            shadow  sudo         system-auth         system-login         useradd
chfn       chsh      groupmems  kde     other     postgresql  runuser    sddm-autologin  sshd    sudo.pacnew  systemd-user        system-remote-login  userdel
chgpasswd  groupadd  groupmod   login   passwd    rlogin      runuser-l  sddm-greeter    su      su-l         system-local-login  system-services      usermod
[root@melati pam.d]#
```
see? there's something that seems suspicious, `sudo` and `sudo.pacnew`. What are those?
```
[root@melati pam.d]# file sudo
sudo: empty
[root@melati pam.d]# file sudo.pacnew
sudo.pacnew: ASCII text
[root@melati pam.d]# cat sudo.pacnew
#%PAM-1.0
auth            include         system-auth
account         include         system-auth
session         include         system-auth
[root@melati pam.d]#
```
Well, shit actually happened. I don't even know what and how, fuck me if I know
why.

Now, time to clean this by copying that `pacnew` file to the rightful owner, P̶a̶l̶e̶s̶t̶i̶n̶i̶a̶n̶,
`sudo` file.  Next, let's check if everything is in their places.

```
$ sudo -ll
[sudo] wachtwoord voor iaji:
Gebruiker iaji mag de volgende opdrachten uitvoeren op melati:

Sudoers-item:
    RunAsUsers: ALL
     Opdrachten:
        ALL
```

time to sleep.
kthxbye.
