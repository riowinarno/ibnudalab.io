(Bahasa) Nggambar Kibor
2019-11-27 23:45:06.08400259 UTC
Post

OK, saya denger abang mau menggambar pcb keyboard yang bisa diprogram menggunakan
QMK.

Sebelum membahas beberapa hal yang akan menjadi dasar pertimbangan dalam menggambar
pcb, Kibor QMK sendiri harus dipaparkan secara umum.

Komponen utama kibor mekanik dengan QMK:

- Mikrokontroler dan komponen pendukung.
  Bagian ini befungsi sebagai penerjemah atas arus listrik yang mengalir saat pencetan
  dipencet.
- Pencetan.
  Saat pencetan dipencet, akan ada arus yang mengalir dari satu pin ke pin lainnya.
  Dan karena ada arus yang mengalir, mikrokontroler bisa menerjemahkan arti dari
  kejadian 'mencet pencetan' dan mengirimkannya ke komputer / henpon.
- Sambungan pencetan.
  Terus terang, kibor yang bisa digunakan sehari-hari tanpa harus melakukan hal-hal
  aneh membutuhkan, setidaknya, sekitar 30-40an pencetan.
  Dan jangan lupa, mikrokontroler yang digunakan qmk tidak ada yang memiliki pin
  i/o lebih dari 30 unit.
  Solusi dari keadaan diatas adalah dengan membuat matriks seperti berikut.
  Yang dengan sederhana mengurangi jumlah pin yang digunakan, yang
  sebelumnya 30 kali 2 menjadi 13 pin saja.

```
     0   1   2   3   4   5   6   7   8   9
     |   |   |   |   |   |   |   |   |   |
 a - q - w - e - r - t - y - u - i - o - p
   - |   |   |   |   |   |   |   |   |   |
 b - a - s - d - f - g - h - j - k - l - ;
   - |   |   |   |   |   |   |   |   |   |
 c - z - x - c - v - b - n - m - , - . - /
```

- Sarung / Wadah.
  Terus terang, bisa saja abang nggak perlu nyambungin pencetan dengan kabel ke
  mikrokontroler.
  Dan saat pengen pencet itu, abang tinggal cari satu satu.
  Sebenernya, sarung / wadah fungsi utamanya adalah memberikan *structural rigidity.*

### Mencari Tujuan

Misalkan abang pengen nggambar pcb kibor 40% dengan beberapa kriteria seperti
"semakin ceper semakin bagus" atau "kibor belah 3 atau 2", tentu harus menyesuaikan
dengan, setidaknya, 3 komponen utama di atas.
Tentu ada kriteria lain yang bisa jadi abang prioritaskan seperti cocok
tidaknya dengan set helm S̶u̶b̶m̶a̶r̶i̶n̶e̶r̶ GMK atau hal hal lain.
Namun, untuk keperluan contoh kali ini, saya akan membuat beberapa kriteria kriteria
yang umum seperti:

- Ceper banget, bang. Tanpa case, bang.
- Bentuk 40%. Karena saya bingung saat punya kebanyakan pencetan.
- Punya pencetan naik turun kiri kanan. Karena banyak yang bilang bahwa
  mereka nggak bisa ngetik tanpa naik turun. lmoa
- Spasi belah. Karena 6.25u terlalu buang buang tempat.
- Berupa 1 persegi panjang. Karena nggambar kibor belah lebih ribet.

Berdasarkan ketiga bagian utama di atas dan kriteria yang telah saya tuliskan,
pertimbangan untuk aktivitas nggambar kali ini adalah sebagai berikut:

- Ketebalan hasil akhir kibor dipengaruhi oleh beberapa hal:
  - Material pcb itu sendiri.
    Praktik yang jamak dilakukan adalah dengan menggunakan material pcb
    dengan ketebalan 1,6mm.
    Cukup kuat dan tidak melengkung bila ditempel 15 pencetan yang
    disusun sejajar dengan lebar 4 pencetan.
  - Sarung, yang biasanya menggunakan plastik cetak ataupun beberapa papan
    akrilik.
    Dikarenakan memang disengaja untuk tidak menggunakan sarung, maka
    bagian ini bisa diabaikan.
    Selain itu, material pcb sudah adekuat dalam memberikan *structural rigidity*
    dalam kondisi pemakaian normal di lingkungan perkantoran.
  - Peletakan mikro kontroler.
    Sangat banyak pcb yang meletakkan mikrokontroler tepat di bawah pencetan.
    Misalkan Let's Split Eh, Iris pra V3, Gherkin, dkk.
    Ada juga yang meletakkan PCB di bagian atas, biasanya desain jepang,
    seperti Corne, Helix, Lily58, dan Kyria.
    Namun, juga banyak yang meletakkan komponen elektronik di bagian bawah
    walaupun dalam bentuk SMD (*onboard*) semacam dz60, kibor korea, gergo, dkk.
- Secara umum, semakin kecil bentuk dari barang ini, semakin sederhana dalam
  mengatur sambungan pencetan di PCB, selama jumlah sambungan tidak terlalu banyak.
- Terus terang, panah tidak ada bedanya dengan pencetan lain.
  Kalaupun ada faktor yang berpotensi untuk menghambat nggambar pcb, mungkin
  hanya masalah peletakan pencetan.
- Bentuk persegi panjang juga tidak memiliki potensi komplikasi.

Berdasarkan pertimbangan-pertimbangan di atas, keputusan yang saya ambil menyoal
desain adalah:

- Menggunakan Promicro dan diletakkan di bagian atas.
  Saya tidak menggunakan SMD dan meletakkan komponen di bagian bawah
  karena terus terang, saya males solder dengan hair dryer.
- Bentuk persegi panjang dengan panjang +/- 13u dan lebar 4u.

### Nggambar Lokasi

Mari kita ke situs [layout editor](http://www.keyboard-layout-editor.com/), bang.
Setelah itu, kita cari tulisan 'preset' dan pilih 'blank layout'.
Abang akan melihat tampilan sebagai berikut:

![Kosong](images/kibor-kle-blank.png)

Setelah itu, pencet panah bawah pada tombol 'add key' dan pilih 'add 10 keys',
dilanjutkan dengan pencet 'add key' tiga kali.
Untuk mengatur lebar dan tinggi dari sebuah pencetan secara manual, abang harus
mencet gambar pencetan di layar, lalu pada bagian 'properties' ubah 'width' atau
'height' untuk mengubah dimensi dari pencetan tersebut.
Sedangkan untuk mengatur lokasi, abang bisa ubah nilai 'x' dan 'y'.

Untuk contoh hasil akhir dari tempel tempelan pencetan di atas, silakan abang
ikuti tautan ke [keyboard-layout-editor](http://www.keyboard-layout-editor.com/#/gists/5fffb34d8de75435990c7294eeb3cf88)
berikut.

Gambar akhir untuk contoh, kurang lebih seperti ini.

![Seperti ini](images/kibor-kle-40.png)

### Menentukan Matriks Pencetan

Setelah abang menentukan gimana layoutnya, sekarang waktunya untuk menentukan matriks
pencetan.
Sebelumnya, abang copy data yang ada pada tab 'Raw Data', lalu silakan abang buka
[kbfirmware](https://kbfirmware.com) dan pastekan data tadi ke bagian 'Paste layout here...'

Tunggu sebentar, lalu abang akan mendapatkan tampilan semacam ini.

![Seperti ini](images/kibor-kbfirmware-4x14.png)

Seperti yang abang lihat, pembuatan matriks tidak terlalu optimum.
Sebagai contoh, hasil generasi dari situs tersebut menentukan bahwa kibor tersebut
memiliki 14 kolom dan 4 baris.
Sedangkan pada baris paling bawah, hanya ada 10an pencetan saja.
Selain itu, ada beberapa kolom yang hanya memiliki 1 atau 2 tombol saja pada kolom
tersebut.

Selain hal semacam itu boros, juga menambah tingkat kerumitan saat nggambar pcb.
Walaupun cuma menambah sedikit kerumitan, tetap saja saya tidak seneng dengan kibor
yang lebih rumit dari seharusnya.

Ok, lanjut dengan yang ada di depan mata.
Untuk menanggulangi hal semacam ini, pilih salah satu pencetan yang terlihat tidak
'lurus'.
Misalkan pencetan 'z' yang ikut kolom '2' di situs tadi.
Abang pilih itu, lalu pada bagian 'change the position of the selected key in the matrix'
ubah nilai 'column' dari '2' menjadi '1'.
Pun halnya untuk tombol 'kanan' yang memiliki nilai 'column' '13', silakan ganti
menjadi '11'.
Lakukan hal tersebut semacam itu hingga matriks menjadi seperti gambar dibawah.

![Gambar matriks ok](images/kibor-kbfirmware-4x12.png)

### Pasang KiCad dan Pustaka Pendukung

Bagian ini gimana, ya.
Sebenernya tinggal ke [kicad-pcb.org](https://kicad-pcb.org) dan pencet 'DOWNLOAD'.
Setelah terdownload, tinggal klik 2 kali, next next next, finish bila di Windows.
Untuk pengguna Linux, tinggal `pacman -Syu kicad --noconfirm` atau perintah semacamnya.
Pengguna apel? Nggak tau.

Saya anggap abang sudah ada kicad terpasang di komputer abang.
Saatnya cari pustaka pendukung seperti *footprint* pencetan dan promicro.
Pertama, abang buka lumbung keeb.io di [github](https://github.com/keebio/Keebio-Parts.pretty).
Selanjutnya, cari pencetan 'clone or download' di layar dan pilih 'download zip.'
Oh, juga unduh pustaka [tmk](https://github.com/tmk/kicad_lib_tmk), dan
[PromicroKicad](https://github.com/Biacco42/ProMicroKiCad) bang.
Setelah selesai, tinggal dulu aja.

Kita lanjutkeun dengan membuka kicad dan klik `File -> New Project`, masukkan nama,
klik enter.
Hasilnya kurang lebih seperti ini.

![Gambar project](images/kibor-kicad-project.png)

Sekarang saatnya mengatur pustaka *footprint* untuk kibor.
Pustaka dari keebio sudah antum unduh, kan?
Silakan dibuka, dan seluruh isinya dimasukkan ke direktory *project* yang barusan 
ntum buat.

![Gambar isi direktori](images/kibor-isi-direktori.png)

Gambar diatas merupakan isi dari direktori yang berisi project tadi.
Harap situ perhatikan bahwa `Keebio-Parts.pretty-master` merupakan isi dari
berkas zip yang telah diunduh dari laman github sebelumnya.

Dilanjutkan dengan 
Silakan antum klik `Preference -> Manage Footprint Library`, dan pilih tab
`Project Specific Libraries`.

![Gambar project specific libraries](images/kibor-kicad-footprint-lib.png)

Kosong, bukan? Nah, silakan antum pencet lambang direktori di sekitaran lambang
plus, minus, keranjang sampah, dan semacamnya.
Situ akan menyaksikan gambar seperti berikut:

![Gambar project specific libraries](images/kibor-kicad-footprint-lib-add.png)

### Nggambar Skematik

Ok, bang. Selanjutnya abang pencet `Nama-Project.sch` di layar kicad abang.
Dilanjutkan dengan dengan menambah `Project Specific Libraries`. 
Caranya sama dengan langkah sebelumnya.

1. Ekstrak berkas `kicad_lib_tmk-master.zip` dan `ProMicroKicad-Master.zip`
   ke sebuah direktori di direktori *project*.
2. Klik `Preferences -> Manage Symbol Libraries`.
3. Klik tab `Project Specific Libaries`.
4. Klik ikon direktori.
5. Klik direktori pada nomor langkah 1.
6. Klik `keyboard_parts.lib`.
7. Klik 'OK'.

Ok, hu. Sekarang suhu sudah siap untuk ekse beberapa `symbol`.
Silakan suhu pencet `Place -> Symbol` lalu arahkan tikus suhu ke sebuah
tempat yang berada di dalam segi empat.
Mana aja boleh sih, hu.
Setelah itu, tinggal klik kiri, dan suhu akan ditampilkan sebuah
jendela yang berisi daftar simbol simbol yang ada.
Silakan suhu arahkan kursor ke penyaring, dan ketikkan 'P̶K̶,̶ ̶T̶H̶R̶E̶A̶D̶ ̶C̶L̶O̶S̶E̶ KEYSW'.
Nah, ada sebiji TO tuh kan, hu.
Suhu pencet itu hasil, lalu klik OK, dan dilanjutkan dengan klik kiri tikus
untuk meletakkan simbol tadi.

![Gambar pilih simbol](images/kibor-kicad-switch.png)

Dilanjutkan dengan menempel simbol dioda.
Langkah yang harus dilakukan sama, namun silakan mencari simbol dengan nama
'D'.
Setelah diletakkan dekat dengan kaki nomor 2, abang harus memutar dioda
tersebut dengan cara mengeklik simbol dioda, lalu tekan pencetan `E` di
kibor abang, lalu ubah orientasi menjadi 180.

![Gambar muter dioda](images/kibor-kicad-dioda-putar.png)

Langkah selanjutnya adalah menempelkan kabel dari kaki `KEYSW` nomor 2
ke kaki `D` nomor 2.
Hal tersebut dapat abang cabai dengan cara:

1. Memencet `Place -> Wire`.
2. Mengarahkan kursor tikus pada kaki `KEYSW` nomor 2.
3. Mengeklik kaki.
4. Menggeser kursor ke arah kaki `D` nomor 2.

![Gambar dioda dan switch](images/kibor-kicad-switch-dioda.png)


Karena kibor yang saya gambar memiliki 46 pencetan, maka daripada mengulang
kedua langkah diatas sebanyak 46 kali, jauh lebih baik bila kita menggunakan
kearifan lokal yaitu *copy-paste*.

Caranya sederhana, abang tinggal klik kiri tikus di sekitaran pasangan
pencetan dan dioda tadi, lalu *drag* sampai semua terpilih.
Setelah terpilih, jangan geser tikus abang.
Tapi cukup tekan Ctrl-C lalu Ctrl-V, dan letakkan sesuai selera.
Walau kalau bisa, jangan terlalu jauh.

Nah, setelah kopi-paste 6 kali, jadilah semacam ini.

![Gambar dioda dan switch sejumlah 48](images/kibor-kicad-switch-dioda-48.png)

Berhubung yang kita butuhkan cuma 46 saja, maka ada dua yang harus dihapus.
Antara lain, pencetan+dioda kolom 5 dan 7, baris ke empat.
Caranya juga kurang lebih sama, *select* pasangan tadi, lalu tekan *delete*
di kibor.

Langkah selanjutnya adalah menempel kabel untuk menyambung semua dioda
bagian kaki nomor 2 pada tiap baris.
Cara gampang sih, bang.
Klik `Place -> Wire`, lalu arahkan kursor tikus ke kaki dioda nomor 2,
lalu klik, geser sampai mendekati kaki dioda nomor 2 di samping kanan (kiri, terserah),
dan saat sudah dekat klik kiri aja tikus abang.

Hasil akhirnya kurang lebih gini, bang.

![Gambar kabel tiap baris](images/kibor-kicad-kabel-baris.png)

Sekarang gantian, nempelin kabel untuk menyambung semua pencetan bagian
kaki nomor 1 pada tiap kolom.
Caranya sama, bang.

Hasilnya kayak gini, bang.

![Gambar kabel tiap kolom](images/kibor-kicad-kabel-kolom.png)

Pada bagian paling atas artikel ini, tiap baris dan kolom kan menyambung
ke pin i/o mikrokontroler kan, bang.
Ada baiknya abang tempel beberapa label global agar gampang.
Caranya juga gampang, bang.
Abang pencet `Place -> Global Label`, lalu arahkan kursor tikus ke sekitar
kolom / baris yang abang ingin tempeli, lalu isi nama dan nomor, lalu
tekan enter, dan diakhiri dengan meletakkan label tersebut dan disambungkan
ke kolom / baris target.

![Gambar global label](images/kibor-kicad-global-label.png)

Ok, secara umum matriks sudah selesai.
Saatnya menempelkan mikrokontroler ke skematik, bang.
Caranya masih sama, `Place -> Symbol`, cari 'Promicro', lalu letakkan.

Setelah itu, kita juga butuh untuk menemepel dan menyambungkan label global
untuk menandakan bahwa baris/kolom tertentu terhubung dengan pin

![Gambar Promicro dan label](images/kibor-kicad-promicro-label.png)

Oh, iya bang.
Silakan abang ubah nama tiap pencetan dengan mengeklik
tiap simbol, lalu tekan `e` di kibor, dan ubah nilai `Reference` dengan
yang unik.
Sebagai contoh, untuk pencentan menggunakan skema `K-Baris-Kolom` dan
`D-Baris-Kolom`.

Dengan demikian, skematik dari kibor ini bisa dianggap sudah selesai.
Namun, untuk melanjutkan nggambar pcb, saya mau memberikan gambaran
gimana kicad menentukan seperti apa bentuk lubang yang ada di pcb.
Kasarnya, kicad memilki fitur untuk menentukan bentuk lubang yang tercetak
di pcb menyesuaikan dengan keinginan pengguna dengan mencocokkan nilai
*reference* dengan pustaka lubang *footprint* yang ada.

Untuk mencontohkan, ikuti langkah berikut:

1. Klik `Tools -> Assign Footprints`.
2. Akan muncul jendela yang terdiri dari 3 kolom.
   1. Paling kiri merupakan daftar pustaka bentuk luba.
   2. Tengah merupakan daftar simbol yang ada di skematik abang.
   3. Paling kanan merupakan daftar lubang yang ada pada kolom paling kiri.
3. Klik salah satu simbol di kolom tengah.
4. Klik salah satu pustaka di kolom kiri.
5. Klik dua kali pada salah satu *footprint* lubang di bagian kanan.
   - untuk dioda, gunakan `Keebio-Parts:Diode`.
   - untuk pencetan, gunakan `Keebio-Parts:MX-PCB-xxxH` dimana `XXX`
     sesuai dengan ukuran helm pencetan pada baris dan kolom yang bersangkutan.
6. Ulangi sampai habis.
7. Pencet 'OK'.

![Gambar assigning footprint](images/kibor-kicad-assign-footprint.png)

### Nggambar PCB

Sekarang beneran waktunya untuk nggambar pcb, bang.
Saat window kicad aktif, abang pencet logo ini.

![Gambar kicad pcbnew](images/kibor-kicad-pcbnew.png)

Saat ditanya beneran mau buat `project.kicad_pcb` atau nggak, jawab aja
KASIH DAAAAAH!!!

Setelah window pcbnew muncul, abang lakukan langkah dibawah ini:

1. Klik `Tools -> Update PCB from Schematic...`
2. Bila semua aman terkendali, akan muncul gambar berikut.
![Gambar pcb terupdate](images/kibor-kicad-pcb-updated.png)
3. Pencet `Update PCB`.
4. Muncullah gambar semacam ini.
![Gambar pcb ruwet](images/kibor-kicad-footprint-ruwet.png)


Yang harus abang lakukan sekarang adalah menata semua *footprint*.
Caranya sih gampang, di bagian yang kosong, abang pencet aja `T` dan ketik
nama simbol.
Sebagai contoh, saya mau cari pencetan paling kiri atas.
Langkah yang saya lakukan adalah:

1. Pencet `esc` beberapa kali agar nggak aneh.
2. Pencet `t`.
3. Ketik referensi *footprint* komponen.
   Misalkan `K-0-0` untuk pencetan pojok kiri atas dan `D-0-0` untuk dioda pojok
   kiri atas.
4. Geser tikus ke tempat kosong.
5. Klik kiri tikus.
   Sampai disini, abang sudah menentukan lokasi dari sebuah *footprint* komponens.
   Bisa dilanjutkan dengan merapikan pencetan tersebut secara manual seperti
   langkah berikut.
6. Pencet `e`.
7. Ubah nilai `x` dan `y` agar mudah diingat (untuk ditambah 19).

Hasil penataan mengikuti gambar pada keyboard-layout-editor diatas, bang.
Semacam ini:

![Gambar kibor tertata](images/kibor-kicad-pcb-tertata.png)

Selanjutnya, abang harus meletakkan promicro pada pojok kanan atas semacam ini.
Kurang lebih semacam ini, bang.

![Gambar promicro terletakkan](images/kibor-kicad-pcb-tertata.png)

Oh, jangan lupa untuk menempel `Edge.Cuts`, bang.
Biar sesuai dengan potongan yang diinginkan.

Caranya:

1. Klik `Place -> Line`.
2. Pada dropdown yang biasanya berisi `F.Cu (PgUp)`, diganti menjadi `Edge.Cuts`.
3. Klik pada salah satu titik.
4. Pindah ke titik lainnya.
5. Sampai habis. owkowkokwokowk

Bila sudah selesai, hasil render dari langkah sebelumnya semacam ini.

![Gambar render 3d](images/kibor-kicad-3d-render.png)

Sekarang waktuny routing, bang.
Abang bisa menggunakann auto-router atau routing secara manual.
Saya sendiri lebih seneng routing secara manual dengan beberapa beberapa garis
panduan:

- jaring antara lubang pencetan dengan lubang dioda, selalu saya letakkan di `F.Cu`.
- jaring antar dioda (biasanya baris), juga saya letakkan di bagian `F.Cu`.
- secara umum, jaring antar pencetan (biasanya kolom), saya letakkan di `B.Cu`.
- jaring antara kolom/baris, yang mudah disambungkan, selalu saya letakkann di `F.Cu`.
- jaring sisa (yang susah), selalu saya gunakan kedua sisi (`F.Cu` dan `B.Cu`) dan
  disambung menggunakan `via`.

Lalu, untuk teknis routing sendiri, abang bisa pencet `Route -> Single Track`,
lalu klik kaki dioda / pencetan, dan tarik sampai ke kaki dioda / pencetan tujuan.
Sedangkan untuk `via`, pencet `Place -> Via` dan letakkan di atas jaring.

Untuk contoh hasil routing, silakan lihat gambar di bawah:

![Gambar routing](images/kibor-kicad-routing.png)

Setelah abang yakin bahwa semua tersambung, silakan abang pencet `Inspect -> Design Rules Checker`
dan tekan `Run DRC`.
Bila semua baik baik saja, tidak akan muncul apa apa.
Kalau ada masalah, ya abang selesaikan, entah karena ada yang belum tersambung atau
semacamnya.

Hasil akhir dari render PCB adalah seperti berikut, bang.

![Gambar render pcb](images/kibor-kicad-render-rampung.png)

### Cetak PCB

Untuk cetak PCB, biasanya saya pakai jlcpcb, bang.
Mereka mau menerima berkas gerber (.zip) dan waktu yang dibutuhkan berkisar dari
1 hari sampai 3 hari.

Nah, untuk membuat file gerber, silakan antum buka `pcbnew` dan buka `project.kicad_pcb`.
Setelah itu, `File -> Plot` dan akan muncul window semacam ini.

![Gambar window plot](images/kibor-kicad-plot.png)

Silakan isi file yang saya lingkari merah dengan nama direktori yang ingin abang buat.
Dan daripada pusing soal `Included Layers`, silakan abang centang semua. owkowkowk.
Selanjutnya, tekan `Generate Drill Files..` yang akan direspon oleh kicad dengan
hasil semacam ini.

![Gambar window drill](images/kibor-kicad-drill.png)

Biarkan saja secara default dan pencet `Generate Drill File` dan `Generate Map File`.
Setelah itu, pencet `Close`.
Abang akan kembali ke window `Plot`, tinggal klik `Plot` dilanjutkan dengan `Close`.

Akhirnya, abang buka *file manager* sistem operasi abang, dan buka direktori project tadi.
Buat zip file dari direktori yang abang buat (lingkaran merah).

Bila sudah selesai, silakan abang buka jlcpcb.com dan klik `Quote Now`.
Saat muncul `Add your gerber file`, drag and drop aja, bang.
Nanti akan ketauan harganya.

![Gambar JLCPCB](images/kibor-jlcpcb.png)

Untuk firmware, silakan abang klik [tautan berikut](2019-12-05--bahasa--nulis-firmware.html)