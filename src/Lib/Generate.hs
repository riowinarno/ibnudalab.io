{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE RecordWildCards #-}
module Lib.Generate where

import           Protolude

import           Data.Yaml
import           Text.Mustache

import           Data.Maybe                               ( fromJust )
import qualified CMark                         as CM
import           Data.List                                ( partition )
import qualified Data.Text                     as T
import           Network.URI
import           Data.Time

import           Lib.Rewrite
import           Lib.RSS
import           Lib.Types

-- | a function to get the content from files in the `filepath`
--   directory.
--   it works by listing the files in a directory
--   followed by mapping the contents using `readRawpost`.
getRaws :: FilePath -> IO [Rawpost]
getRaws filepath = getFiles filepath >>= \p -> do
  putText "Raw files:"
  mapM_ (\x -> putText $ "\t - " <> T.pack x) p
  mapM (readRawpost filepath) p

-- | generating the whole blog.
generateBlog :: SiteInfo -> IO ()
generateBlog SiteInfo {..} = do
  rawposts <- getRaws siteinfoFiles
  putText "\nRead every files in the info files."
  maintemplate  <- compileMustacheDir "main" siteinfoTemplatesDir
  indextemplate <- compileMustacheDir "index" siteinfoTemplatesDir
  css           <- readFile siteinfoCSS
  let (posts, pages) = partition (\x -> rawpostType x == Post) rawposts
      links          = map mkMenuLink pages
      indexlinks     = map mkIndexLink $ reverse . sort $ posts
      index = Index "Home" css "Description of homepage" links indexlinks
      indexpage      = render indextemplate index
  putText "Writing into public dir:"
  forM_ rawposts $ \Rawpost {..} -> do
    let htmlname = htmlFilename (T.unpack rawpostTitle) rawpostDate
        page     = render maintemplate $ mkPage css links Rawpost {..}
    writeFile (siteinfoPublic ++ "/" ++ htmlname) page
    putText $ "\t - " <> T.pack htmlname
  putText "Writing index file."
  writeFile (siteinfoPublic ++ "/index.html") indexpage
  putText "Fin."

-- | rendering the mustache template to a html text.
render :: ToJSON a => Template -> a -> Text
render template a = toStrict $ renderMustache template $ toJSON a

-- | creating page by taking css, links, and the rawposts.
--   the links are usually page link.
--   the rawpost, well, the content of the markdown file.
mkPage :: Text -> [BlogLink] -> Rawpost -> BlogPage
mkPage css links Rawpost {..} =
  let desc    = T.take 100 rawpostTitle
      content = CM.commonmarkToHtml [] rawpostContent
  in  BlogPage rawpostTitle css desc content links

-- | creates a menu link based on the rawpost.
mkMenuLink :: Rawpost -> BlogLink
mkMenuLink Rawpost {..} =
  let htmlname = T.pack $ htmlFilename (T.unpack rawpostTitle) rawpostDate
  in  BlogLink htmlname rawpostTitle

-- | creates an index link based on the rawpost.
mkIndexLink :: Rawpost -> BlogLink
mkIndexLink Rawpost {..} =
  let formatteddate =
        T.pack $ formatTime defaultTimeLocale "%z%F" $ utctDay rawpostDate
      title    = formatteddate <> " - " <> rawpostTitle
      htmlname = T.pack $ htmlFilename (T.unpack rawpostTitle) rawpostDate
  in  BlogLink htmlname title

-- | generating feed.
generateFeed :: SiteInfo -> IO ()
generateFeed SiteInfo {..} = do
  now  <- getCurrentTime
  raws <- getRaws siteinfoFiles
  let
    rawposts = reverse . sort $ filter (\x -> rawpostType x == Post) raws
    rssTitle = T.unpack siteinfoName
    rssLink  = fromJust . parseURI . T.unpack $ siteinfoUrl
    rssDesc  = "Nothing Unusual."
    rssElem =
      [ Language "en-us"
      , ManagingEditor (T.unpack siteinfoAuthor)
      , WebMaster (T.unpack siteinfoAuthor)
      , ChannelPubDate now
      , Generator
        "https://gitlab.com/ibnuda/ibnuda.gitlab.io/blob/master/src/Lib/RSS.hs"
      ]
    rssItem = map makeItems rawposts
  writeGenerated siteinfoPublic siteinfoRSS
    . T.pack
    . showXML
    . rssToXML
    $ RSS {..}
 where
  makeItems Rawpost {..} =
    [ makeTitle rawpostTitle
    , makeLink Rawpost {..}
    , makeAuthor siteinfoAuthor
    , PubDate rawpostDate
    , Description
      (T.unpack . CM.commonmarkToHtml [] $ T.take 1000 rawpostContent)
    ]
  makeTitle = Title . T.unpack
  makeLink Rawpost {..} =
    Link
      .  fromJust
      .  parseURI
      $  T.unpack siteinfoUrl
      ++ "/"
      ++ htmlFilename (T.unpack rawpostTitle) rawpostDate
  makeAuthor = Author . T.unpack
