{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE RecordWildCards #-}
module Lib.Types where

import           Protolude

import           Data.Text
import           Data.Time
import           Data.Time.Clock.POSIX
import           Data.Yaml

-- | site information.
data SiteInfo = SiteInfo
  { siteinfoUrl          :: Text -- ^ main url.
  , siteinfoName         :: Text -- ^ site's name.
  , siteinfoAuthor       :: Text -- ^ site's author.
  , siteinfoFiles        :: Filename -- ^ directory where the post and page files reside.
  , siteinfoPublic       :: Filename -- ^ direcotry where the html being put.
  , siteinfoRSS          :: Filename -- ^ name of the feed.xml.
  , siteinfoCSS          :: Filename -- ^ css filename.
  , siteinfoTemplatesDir :: Filename -- ^ template file.
  } deriving (Show, Eq)

-- | standard instance of yaml.
instance FromJSON SiteInfo where
  parseJSON = withObject "SiteInfo" $ \o -> do
    siteinfoUrl <- o .: "url"
    siteinfoName <- o .: "name"
    siteinfoAuthor <- o .: "author"
    siteinfoFiles <- o .: "files"
    siteinfoPublic <- o .: "public"
    siteinfoRSS <- o .: "rss"
    siteinfoCSS <- o .: "css"
    siteinfoTemplatesDir <- o .: "templates"
    return SiteInfo {..}

-- | whenever user generates the site, the program should read the config file.
--   when it can read and parse the file, it will return the config above.
--   otherwise, it will stop the world.
readSiteinfo :: IO SiteInfo
readSiteinfo = do
  eith <- decodeFileEither "res/config.yaml"
  case eith of
    Left  e -> panic . pack . prettyPrintParseException $ e
    Right s -> pure s

-- | title is a text.
type Title = Text
-- | filename is a filepath. the fuck did i think about it?
type Filename = FilePath
-- | well, the content of the markdown file should be a text.
type MdContent = Text
-- | there are only two kind of post. a normal post and a page.
--   it derives `Eq`, `Read`, `Show` for shit and giggle.
data PostType = Post | Page deriving (Eq, Read, Show)

-- | how the post should be structured.
data Rawpost = Rawpost
  { rawpostFilename :: Filename -- ^ it should have a filename. of course it should have. what the fuck did i think?
  , rawpostTitle    :: Title -- ^ title.
  , rawpostDate     :: UTCTime -- ^ date created.
  , rawpostType     :: PostType -- ^ post type.
  , rawpostContent  :: MdContent -- ^ the content itself.
  } deriving (Eq, Show)

-- | just an empty post. doesn't derive from monoid because why the fuck do you want to join the post?
defaultRawpost :: Rawpost
defaultRawpost = Rawpost "" "" (posixSecondsToUTCTime 0) Post ""

-- | standard instance because i want to order them by date.
instance Ord Rawpost where
  compare a b = compare (rawpostDate a) (rawpostDate b)

-- | think of an a tag in html.
data BlogLink = BlogLink
  { menulinkurl   :: Text -- ^ the url of the link.
  , menulinktitle :: Text -- ^ what should be shown in the link.
  } deriving (Generic)

-- | standard instance.
instance ToJSON BlogLink

-- | the structure of a blog page.
data BlogPage = BlogPage
  { pagetitle   :: Text -- ^ should have title.
  , pagecss     :: Text -- ^ should have css.
  , pagedesc    :: Text -- ^ why tho?
  , pagecontent :: Text -- ^ pretty obvious.
  , pagelinks   :: [BlogLink] -- ^ there are links in the page.
  } deriving (Generic)

-- | standard instance.
instance ToJSON BlogPage

-- | front page.
data Index = Index
  { indextitle   :: Text -- ^ the title of the index. should be obvious, obviously.
  , indexcss     :: Text -- ^ the css.
  , indexdesc    :: Text -- ^ description.
  , indexlinks   :: [BlogLink] -- ^ post links.
  , indexindice  :: [BlogLink] -- ^ page links.
  } deriving (Generic)

-- | standard instance.
instance ToJSON Index
