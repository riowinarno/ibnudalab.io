{-# LANGUAGE RecordWildCards #-}
module Lib.Rewrite where

import           Protolude                         hiding ( intercalate )

import           Data.Char
import qualified Data.Text                     as T
import           Data.Time
import           System.Directory
import           System.FilePath.Posix
import qualified Text.Read                     as TR
import           Text.Regex                               ( mkRegex
                                                          , subRegex
                                                          )

import           Lib.Types

-- | used for file-to-`Rawpost` transformation.
--   it works by combining the directory name and the filename,
--   followed by parsing the lines into `Rawpost`.
--   if the file cannot be parsed into `Rawpost` it will
--   throw a `panic`
readRawpost :: FilePath -> Filename -> IO Rawpost
readRawpost path nameoffile = do
  content <- readFile $ path </> nameoffile
  case T.lines content of
    title : date : tipe : writing -> do
      return $ Rawpost nameoffile
                       title
                       (TR.read $ T.unpack date)
                       (TR.read $ T.unpack tipe)
                       (T.unlines writing)
    _ -> panic "Invalid file."

-- | used for getting the list of files in a directory.
--   this function will skip any directories in the `path`.
getFiles :: FilePath -> IO [FilePath]
getFiles path =
  withCurrentDirectory path $ getDirectoryContents "." >>= filterM doesFileExist

-- | used for file deletion in the given directory.
deleteFiles :: FilePath -> IO ()
deleteFiles path = getFiles path >>= mapM_ (removeFile . (path </>))

-- | creating filename for the markdown file.
--   it will remove any non alphanumeric characters and replace them
--   with dashes.
mdFilename :: [Char] -> [Char]
mdFilename input = subRegex (mkRegex "[^a-zA-Z0-9_.]") input "-"

-- | creating filename for the html file.
--   it will remove any non alphanumeric characters and replace them
--   with dashes and followed by prepending a date in front of the name.
htmlFilename :: [Char] -> UTCTime -> [Char]
htmlFilename title date =
  let fn = mdFilename title
      dt = formatTime defaultTimeLocale "%z%F" $ utctDay date
  in  map toLower $ dt ++ "-" ++ fn ++ ".html"

-- | writing `Rawpost` into a file in the `siteinfofiles` directory.
--   the content of the aforementioned file is:
--
--   - `rawpostTitle`
--   - `rawpostDate`
--   - `rawpostType`
--   - `rawpostContent`
--
--   in that order
writeRawpost :: FilePath -> Rawpost -> IO ()
writeRawpost siteinfofiles Rawpost {..} = writeFile
  (siteinfofiles </> rawpostFilename)
  (T.unlines [rawpostTitle, show rawpostDate, show rawpostType, rawpostContent])

-- | writing the generated html content into the public directory in a file
--   with `filename`.
writeGenerated :: FilePath -> Filename -> Text -> IO ()
writeGenerated siteinfopublic filename content =
  writeFile (siteinfopublic </> filename) content

-- | creating a markdown for the post. it takes three params, where the
--   last two were provided by the user (intended usage).
createMdFile :: SiteInfo -> [Char] -> Title -> IO ()
createMdFile SiteInfo {..} ctype title = do
  now <- getCurrentTime
  let fn = mdFilename (T.unpack title)
      ct = TR.read ctype
  writeRawpost siteinfoFiles $ Rawpost (fn ++ ".md") title now ct "Write here."
